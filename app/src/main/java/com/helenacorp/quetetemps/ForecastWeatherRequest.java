package com.helenacorp.quetetemps;

import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.octo.android.robospice.request.googlehttpclient.GoogleHttpClientSpiceRequest;

import java.io.IOException;

import roboguice.util.temp.Ln;

/**
 * Created by helena on 26/06/2017.
 */

public class ForecastWeatherRequest extends GoogleHttpClientSpiceRequest<ForecastWeatherModel> {
    private String baseUrl;


    public ForecastWeatherRequest(double latitude, double longitude, String apiKey) {
        super( ForecastWeatherModel.class );
        this.baseUrl = "http://api.openweathermap.org/data/2.5/forecast?lat="+latitude+"&lon="+longitude+"&APPID="+apiKey+"&lang=fr"+"&units=metric";
    }

    @Override
    public ForecastWeatherModel loadDataFromNetwork() throws IOException {
        Ln.d( "Call web service " + baseUrl );
        HttpRequest request = getHttpRequestFactory()//
                .buildGetRequest( new GenericUrl( baseUrl ) );
        request.setParser( new GsonFactory().createJsonObjectParser() );
        return request.execute().parseAs( getResultType() );
    }
}

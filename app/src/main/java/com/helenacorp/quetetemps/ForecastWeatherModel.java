package com.helenacorp.quetetemps;

import com.google.api.client.util.Key;

import java.util.ArrayList;

import static android.R.attr.id;

public class ForecastWeatherModel {

    @Key private String cod;
    @Key private Double message;
    @Key private Integer cnt;
    @Key private java.util.List<com.helenacorp.quetetemps.List> list = new ArrayList<com.helenacorp.quetetemps.List>();
    @Key private City city;
    @Key private int content;

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public Double getMessage() {
        return message;
    }

    public void setMessage(Double message) {
        this.message = message;
    }

    public Integer getCnt() {
        return cnt;
    }

    public void setCnt(Integer cnt) {
        this.cnt = cnt;
    }

    public java.util.List<com.helenacorp.quetetemps.List> getList() {
        return list;
    }

    public void setList(java.util.List<com.helenacorp.quetetemps.List> list) {
        this.list = list;
    }

    public City getCity() {
        return city;
    }

    public void setCity() {
        this.city = city;
    }

    public int getId() {
        return id;
    }

    public int getContent() {
        return content;
    }

    public void setContent(int content) {
        this.content = content;
    }


}

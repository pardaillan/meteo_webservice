package com.helenacorp.quetetemps;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.icu.util.Calendar;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.octo.android.robospice.GsonGoogleHttpClientSpiceService;
import com.octo.android.robospice.SpiceManager;
import com.octo.android.robospice.persistence.exception.SpiceException;
import com.octo.android.robospice.request.listener.RequestListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST = 1;
    protected SpiceManager spiceManager = new SpiceManager(GsonGoogleHttpClientSpiceService.class);
    LocationManager locationManager = null;
    LocationListener locationListener;
    TextView cityText, tempText, dateText;
    ImageView iconWeather;
    private String apiKey;
    private double latitude;
    private double longitude;
    private ArrayList<List> forecastList = new ArrayList<>();
    private RecyclerView recyclerView;
    private ForecastRecyclerView mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        apiKey = getString(R.string.apiKey);

        cityText = (TextView) findViewById(R.id.city);
        dateText = (TextView) findViewById(R.id.time_current);
        tempText = (TextView) findViewById(R.id.temperature);
        iconWeather = (ImageView) findViewById(R.id.icon_weather);

        tempText.setTypeface(Typeface.createFromAsset(MainActivity.this.getAssets(), "font/crapaud_gros.ttf"));
        cityText.setTypeface(Typeface.createFromAsset(MainActivity.this.getAssets(), "font/crapaud_gros.ttf"));
        dateText.setTypeface(Typeface.createFromAsset(MainActivity.this.getAssets(), "font/crapaud_gros.ttf"));
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        locationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);


        locationListener = new LocationListener() {
            public void onLocationChanged(Location location) {
                latitude = location.getLatitude();
                longitude = location.getLongitude();
                performRequest();

                // Toast.makeText(MainActivity.this, "Latitude = " + location.getLatitude() + " Longitude = " + location.getLongitude(),
                // Toast.LENGTH_LONG).show();
            }

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }

        };

    }

    @Override
    protected void onStart() {
        spiceManager.start(this);
        super.onStart();


        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION},
                    MY_PERMISSIONS_REQUEST);
            return;

        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 50, locationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 50, locationListener);
    }


    @Override
    protected void onStop() {
        spiceManager.shouldStop();
        super.onStop();
        locationManager.removeUpdates(locationListener);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 10, locationListener);
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 10, locationListener);
    }

    private void performRequest() {

        ForecastWeatherRequest request = new ForecastWeatherRequest(latitude, longitude, apiKey);
        spiceManager.execute(request, new ForecastWeatherRequestListener());

        CurrentWeatherRequest requestC = new CurrentWeatherRequest(latitude, longitude, apiKey);
        spiceManager.execute(requestC, new CurrentWeatherRequestListener());
    }

    //inner class of your spiced Activity
    private class ForecastWeatherRequestListener implements RequestListener<ForecastWeatherModel> {

        @Override
        public void onRequestFailure(SpiceException e) {
            //update your UI


        }

        @Override
        public void onRequestSuccess(ForecastWeatherModel forecastWeatherModel) {

            forecastList = (ArrayList<List>) forecastWeatherModel.getList();
            mAdapter = new ForecastRecyclerView(forecastList, MainActivity.this);
            // use a linear layout manager
            mLayoutManager = new LinearLayoutManager(MainActivity.this);
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(mAdapter);

        }
    }

    private class CurrentWeatherRequestListener implements RequestListener<CurrentWeatherModel> {

        @Override
        public void onRequestFailure(SpiceException spiceException) {
            Toast.makeText(MainActivity.this, "sorry wait plz",
                    Toast.LENGTH_LONG).show();

        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        @Override
        public void onRequestSuccess(CurrentWeatherModel currentWeatherModel) {

            cityText.setText(currentWeatherModel.getName().toString());
            dateText.setText(getDate(currentWeatherModel.getDt("EEE, d MMM YYYY")));

            tempText.setText(currentWeatherModel.getMain().getTemp().toString() + "°");
            Glide.with(MainActivity.this)
                    .load("http://openweathermap.org/img/w/" + currentWeatherModel.getWeather().get(0).getIcon() + ".png")
                    .into(iconWeather);
        }

        @RequiresApi(api = Build.VERSION_CODES.N)
        public String getDate(long milliSeconds) {
            milliSeconds = System.currentTimeMillis();
            // Create a DateFormatter object for displaying date in specified format.
            SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM yyyy");

            // Create a calendar object that will convert the date and time value in milliseconds to date.
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(milliSeconds);
            return formatter.format(calendar.getTime());

        }

    }

}

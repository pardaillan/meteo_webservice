package com.helenacorp.quetetemps;

import android.content.Context;
import android.icu.util.Calendar;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Created by helena on 28/06/2017.
 */

public class ForecastRecyclerView extends RecyclerView.Adapter<ForecastRecyclerView.MyViewHolder> {
    Context context;
    private ArrayList<List> listForecast;

    public ForecastRecyclerView(ArrayList<List> listForecast, Context context) {
        this.listForecast = listForecast;
        this.context = context;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static String getDate(long milliSeconds) {

        SimpleDateFormat formatter = new SimpleDateFormat("EEE, d MMM    HH:mm");

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds * 1000);
        return formatter.format(calendar.getTime());

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_weather, parent, false);
        return new MyViewHolder(itemView);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        List f = listForecast.get(position);
        holder.cityF.setText(f.getWeather().get(0).getDescription().toString());
        holder.temperature.setText(f.getMain().getTemp().toString() + "°C");
        holder.time.setText(getDate(f.getDt()));
        Glide.with(context).load("http://openweathermap.org/img/w/" + f.getWeather().get(0).getIcon() + ".png").into(holder.iconF);
    }

    @Override
    public int getItemCount() {
        return listForecast.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView cityF, temperature, time;
        public ImageView iconF;

        public MyViewHolder(View itemView) {
            super(itemView);
            cityF = (TextView) itemView.findViewById(R.id.description_forecast);
            temperature = (TextView) itemView.findViewById(R.id.temp_forecast);
            iconF = (ImageView) itemView.findViewById(R.id.icon_f);
            time = (TextView) itemView.findViewById(R.id.dt);

        }
    }
}

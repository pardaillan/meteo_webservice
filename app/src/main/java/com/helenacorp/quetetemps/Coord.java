package com.helenacorp.quetetemps;


import com.google.api.client.util.Key;

public class Coord {

    @Key private Double latitude;
    @Key private Double longitude;

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

}